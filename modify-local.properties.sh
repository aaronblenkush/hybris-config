conf="/apps/dmmc/hybris/config/local.properties"
confNew="$conf.new"
confBak="$conf.bak"
key=$1
value=$2
pair="$key=$value"

cp "$conf" "$confNew"

sedKey=$(echo "$key" | sed -e "s/\\./\\\\./g")
expr='/'$sedKey'/c'$pair

sed -i "$expr" "$confNew"

diff "$conf" "$confNew"
rm -f "$confNew"
while true; do
    read -p "Does the diff look correct? 'Yes' will write the changes. (y/n): " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "Sed expression: $expr"

echo "Creating backup: $confBak"
cp "$conf" "$confBak"

echo "Writing changes to: $conf"
sed -i "$expr" "$conf"

