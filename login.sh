##
# Configuration

server=$1
#server="prodhybris01v.dm.projecta.com:9001"
hac="$server/hac"

##
# Log in

url="$hac/j_spring_security_check"

jsessionid=$( \
    curl -i "$url" \
    --data 'j_username=admin&j_password=p4s5w0rd' \
    --compressed \
    2>/dev/null \
    |\
    grep -Po "(?<=JSESSIONID=)[a-zA-Z0-9]+(?=\\b)" \
)
echo "$jsessionid"

