##
# Configuration


# Example: set config change on all nodes:
# > for node in {1..3}; do ./configupdate.sh "prodhybris0${node}v.dm.projecta.com:9001" "key" "value"; done


source "login.sh"

if [ "$1" == "--help" ]; then
    echo "Usage: ./config-update.sh <server> <key> <value>"
    echo "Example: ./config-update.sh 'devhybris01v.dm.projecta.com' 'some.key' 'Some value'"
    exit 0
fi


key=$2
value=$3
#key="avatax.company.code"
#value="DMAW"

##
# Config update
url="$hac/platform/configupdate/"

curl -sD - "$url" \
    -H "Cookie: JSESSIONID=$jsessionid" \
    --data "key=$key&val=$value" \
    --compressed \
    -o /dev/null
echo ""
